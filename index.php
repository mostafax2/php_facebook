<?php
    include_once 'SQLobject.php';
    session_start();
    if(isset($_POST['ValueExist']))
    {
        $db= new SQLobject;
        if(!$db->qaryExisting("users", $_POST['Filed'], $_POST['ValueExist']))
        {
            die($_POST['Filed'].' notExist');
        }
    }  else{
//    else if(isset($_POST['Email'])){
//        
//        $db= new SQLobject;
//        $db->addOnlineUser();
//        if(!empty($_SESSION['addUserError'])){
//            header('Location: register.php');
//            echo $_SESSION['addUserError'];
//            $_SESSION['addUserError']="";
//        } 
//        else{
//            header('Location: main.php');
//            $_SESSION['RegisterSuccesly']="you Registerd Succesly please log in";
//        }
//
//    }  else {
         
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Hello</title>
        <link type="text/css" rel="stylesheet" href="register.css">
        <script type="text/javascript" src="js/libs/jquery/jquery.js"></script>
        <script type="text/javascript" src="js/libs/myFunction.js"></script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <script>
            $(document).ready(function (){
                
                $('#regForm').submit(function (){
                    ret=true;
                    if($('#Email').hasClass('errorHighlight'))
                    {
                        ret = false;
                    } else if($.trim($('#Email').val())===""){
                        addError('#Email','#EmailError');
                        $('#EmailError').html("Invalid email. must be non-empty.");
                        ret = false;
                    }
                    if($('#password').hasClass('errorHighlight'))
                    {
                        ret = false;
                    }else if($.trim($('#password').val())===""){
                        addError('#password','#PasswordError');
                        $('#PasswordError').html("Invalid password. must be non-empty.");
                        ret = false;
                    }
                    return ret;
                });                
            });
            function validatePasswor()
            {
                
                var pass=$.trim($('#password').val());
                if(pass==="")
                    return;
                if($.trim($('#Email').val())==="")
                {
                   addError('#password','#PasswordError');
                   $('#PasswordError').html("please file the email first");
                   return; 
                }
                var filter=new RegExp(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{8,})$/);
                if(!filter.test(pass))
                {
                    addError('#password','#PasswordError');
                    $('#PasswordError').html("Invalid password. Length should be at least 8 characters,<br> and contain at least one letter and one digit.");
                    return;
                }
                checkingIfAValueExists("password",pass,'#password','#PasswordError');
            }
            function validateEmail()
            {
               
                var email=$.trim($('#Email').val());
                if(email==="")
                    return;
                var filter = new RegExp(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
                if(!filter.test(email))
                {
                    addError('#Email','#EmailError');
                    $('#EmailError').html("Invalid email address. Should be characters@text.text");
                    
                    return;
                }
                checkingIfAValueExists("email",email,'#Email','#EmailError');
            }
            function checkingIfAValueExists(filde,email,inputID,errorID)
            {
                $.post('index.php', {Filed :filde,ValueExist:email }, function(data) {
                    if(data===filde+" notExist")
                    {
                        addError(inputID,errorID);
                        $(errorID).html("The "+ filde+" is not exist");
                    }
                });
            }    
       </script>
    <?php
        if(isset($_SESSION['Massage'])&&($_SESSION['Massage']=="registered")){
    ?>
       <script>
         $(document).ready(function (){ 
            $('#walcomMassage').css('visibility','visible');
            $('#walcomMassage').html("You registered please log in");
        });
       </script>
    <?php
        $_SESSION['Massage']="";
        }
    ?>
       
    </head>
    <body>
         <div class="ui-widget">
            <div class="logo">
               <div class="form-title">
               
               </div>
           </div>
            <div id="div-regForm">
                <div class="form-title">Log in</div>
                <form id="regForm" action="index.php"  method="POST">
                    <div class="input-container">
                        <span id="walcomMassage" class="walcomMassage"></span>
                    </div>
                    <div class="input-container">
                        <input type="text" id="Email" name="Email" placeholder="example@abc.com" onblur="validateEmail()" onfocus="removeError('#Email','#EmailError')" />
                        <span id="EmailError" class="error"></span>
                    </div>
                    <div class="input-container">
                        <input id="password" type="text" name="Password" placeholder="password" onblur="validatePasswor()" onfocus="removeError('#password','#PasswordError')">
                        <span id="PasswordError" class="error"></span>
                    </div>
                    <div class="input-container">
                        <input type="submit" value="login" class="greenButton">
                    </div>
                </form> 
                <div>
                    <br><span class="form-title">New user?</span><br>
                     <div class="input-container">
                        <button onclick="window.location.href='register.php'" class="lightBlueButton">Register</button>
                     </div>
                </div>
            </div>
         </div>
         <?php
                
            }
        ?>
        
    </body>
</html>
